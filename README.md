## What is done
1) Functionality of task manager using function components including Context API as state management

2) Typescript as a base. It has enums and interfaces.

3) Optimization for mobile devices, desktop. It has adaptive styles.

4) Test coverage using @testing-library/react.

5) Sass’s module system

## What should be improved
1) I would improve validation for a new task and an edited task

2) I would also add notifications

3) I would also add reducer in case expansion of functionality

4) Documentation for react components, for example Storybook

5) UI library

6) Memoization in future. No time-consuming functions right now

## Setup a project
### `yarn`

### `yarn start`

## Available Scripts

In the project directory, you can run:

### `yarn start`

Runs the app in the development mode.\
Open [http://localhost:3000](http://localhost:3000) to view it in the browser.

The page will reload if you make edits.\
You will also see any lint errors in the console.

### `yarn test`

Launches the test runner in the interactive watch mode.\
See the section about [running tests](https://facebook.github.io/create-react-app/docs/running-tests) for more information.
