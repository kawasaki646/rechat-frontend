export enum Status {
    TODO = 'todo',
    IN_PROGRESS = 'in progress',
    IN_QA = 'in qa',
    DONE = 'done',
    DEPLOYED = 'deployed',
    BLOCKED = 'blocked'
}

export enum Nav {
    EMPTY = '',
    HOME = 'home',
    EDIT = 'edit task'
}