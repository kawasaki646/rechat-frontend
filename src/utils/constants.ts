export const NO_TASKS_MESSAGE = 'You have nothing to do.'
export const SLEEP_MESSAGE = 'Go get some sleep.'
export const HOME_PAGE = 'Task Management'