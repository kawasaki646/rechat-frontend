import {Nav, Status} from "./enums";

export interface Task {
    id: number,
    title: string,
    description: string,
    status: Status
}

export interface Context {
    tasks: Task[],
    nav: Nav
}

export interface SelectOption {
    value: string,
    label: string
}