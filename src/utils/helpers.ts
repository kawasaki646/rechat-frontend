import {Status} from "./enums";

export function capitalizeFirstLetter(string) {
    return string.charAt(0).toUpperCase() + string.slice(1);
}

export function fetchStatusesForSpecificStatus(status: Status) {
    let result: Status[] = []
    switch (status) {
        case 'todo': result = [Status.IN_PROGRESS]; break;
        case 'in progress': result = [Status.IN_QA, Status.BLOCKED]; break;
        case 'in qa': result = [Status.TODO, Status.DONE]; break;
        case 'blocked': result = [Status.TODO]; break;
        case 'done': result = [Status.DEPLOYED]; break;
        default: break;
    }

    return result;
}
