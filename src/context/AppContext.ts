import React from 'react';
import {Context} from "../utils/interfaces";

export const context = {
    tasks: [],
    nav: ''
};

export const TaskContext = React.createContext<Context>(
    context as Context
);