import React, {useState} from 'react';
import {TaskContext} from "./context/AppContext";
import './App.scss';
import {HOME_PAGE} from "./utils/constants";
import {Context, Task} from "./utils/interfaces";
import {Nav} from "./utils/enums";
import {BrowserRouter as Router, Route, Routes,} from "react-router-dom";
import MainPage from "./pages/MainPage";
import EditPage from "./pages/EditPage";
import {capitalizeFirstLetter} from "./utils/helpers";

function App() {
    const [context, setContext] = useState<Context>({tasks: [] as Task[], nav: Nav.HOME as Nav});

    const handleAddNewTask = (task: Task) => {
        setContext({
            tasks: context.tasks.concat([task]),
            nav: Nav.HOME
        });
    }

    const onEditTask = (task: Task) => {
        const tasksCopy = [...context.tasks];
        const targetIndex = tasksCopy.findIndex(value => task.id === value.id);
        tasksCopy.splice(targetIndex, 1, task);
        setContext({
            tasks: tasksCopy,
            nav: Nav.HOME
        });
    }

    const onCancelTask = () => {
        setContext({
            tasks: context.tasks,
            nav: Nav.HOME
        });
    }

    const handleNavigateToTask = () => {
        setContext({
            tasks: context.tasks,
            nav: Nav.EDIT
        });
    }

    return (
        <Router>
            <TaskContext.Provider value={context}>
                <header
                    className="App-header"
                    data-testid="App-header"
                >
                    <div>{HOME_PAGE} &gt; {capitalizeFirstLetter(context.nav)}</div>
                </header>
                <div
                    className="App"
                    data-testid="App"
                >
                    <Routes>
                        <Route path="/" element={
                            <MainPage
                                onAddNewTask={(task) => {
                                    handleAddNewTask(task)
                                }}
                                onNavigateToTask={handleNavigateToTask}
                            />
                        }/>
                        <Route path="edit/:idx" element={
                            <EditPage
                                onEdit={onEditTask}
                                onCancel={onCancelTask}
                            />
                        }/>
                    </Routes>
                </div>
            </TaskContext.Provider>
        </Router>
    );
}

export default App;
