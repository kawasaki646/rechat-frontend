import React from 'react';
import { render, screen } from '@testing-library/react';
import App from './App';

test('Should render application header and body ', () => {
  render(<App />);
  const header = screen.getByTestId('App-header');
  const body = screen.getByTestId('App');
  expect(header).toBeInTheDocument();
  expect(body).toBeInTheDocument();
});
