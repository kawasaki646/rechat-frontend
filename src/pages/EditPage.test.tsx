import React from 'react';
import {render, screen} from '@testing-library/react';
import EditPage from './EditPage';
import {MemoryRouter} from "react-router-dom";

test('Should render EditTaskComponent', () => {
    render(
        <MemoryRouter>
            <EditPage
                onEdit={() => {}}
                onCancel={() => {}}
            />
        </MemoryRouter>
    );
    const editTaskForm = screen.getByTestId('edit-task-form');
    const titleField = screen.getByTestId('title-field');
    const descriptionField = screen.getByTestId('description-field');
    const statusField = screen.getByTestId('status-field');
    const editNewTask = screen.getByTestId('edit-new-task');
    const cancelNewTask = screen.getByTestId('cancel-new-task');
    expect(editTaskForm).toBeInTheDocument();
    expect(titleField).toBeInTheDocument();
    expect(descriptionField).toBeInTheDocument();
    expect(statusField).toBeInTheDocument();
    expect(editNewTask).toBeInTheDocument();
    expect(cancelNewTask).toBeInTheDocument();
});
