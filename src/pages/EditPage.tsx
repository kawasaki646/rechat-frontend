import React from 'react';
import EditTaskComponent from "../components/EditTaskComponent/EditTaskComponent";

function EditPage({onEdit, onCancel}) {
    return (
        <div
            id="edit-page"
            data-testid="edit-page"
        >
            <EditTaskComponent
                onEdit={onEdit}
                onCancel={onCancel}
            />
        </div>
    );
}

export default EditPage;
