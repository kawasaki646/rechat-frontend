import React from 'react';
import {render, screen} from '@testing-library/react';
import MainPage from './MainPage';
import {MemoryRouter} from "react-router-dom";

test('Should render AddNewTaskComponent and TasksComponent', () => {
    render(
        <MemoryRouter>
            <MainPage
                onAddNewTask={() => {}}
                onNavigateToTask={() => {}}
            />
        </MemoryRouter>
    );
    const addNewTaskForm = screen.getByTestId('add-new-task-form');
    const tasksComponent = screen.getByTestId('tasks-component');
    expect(addNewTaskForm).toBeInTheDocument();
    expect(tasksComponent).toBeInTheDocument();
});
