import React from 'react';
import AddNewTaskComponent from "../components/AddNewTaskComponent/AddNewTaskComponent";
import TasksComponent from "../components/TasksComponent/TasksComponent";

function MainPage({onAddNewTask, onNavigateToTask}) {
    return (
        <div
            id="main-page"
            data-testid="main-page"
        >
            <AddNewTaskComponent
                onAddNewTask={onAddNewTask}
            />
            <TasksComponent
                onNavigateToTask={onNavigateToTask}
            />
        </div>
    );
}

export default MainPage;
