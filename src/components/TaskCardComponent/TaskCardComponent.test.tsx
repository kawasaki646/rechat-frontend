import React from 'react';
import {render, screen} from '@testing-library/react';
import TaskCardComponent from './TaskCardComponent';
import {MemoryRouter} from "react-router-dom";

const TASK_MOCK_DATA = {
    title: 'Title',
    description: 'Description',
    status: 'Status',
    id: 123
}

test('Should render title, description, status', () => {
    render(<MemoryRouter>
            <TaskCardComponent
                task={TASK_MOCK_DATA}
                onNavigateToTask={() => {
                }}
            />
        </MemoryRouter>
    );
    const title = screen.getByTestId('card-title');
    const description = screen.getByTestId('card-description');
    const status = screen.getByTestId('status-label');
    expect(title).toHaveTextContent(TASK_MOCK_DATA.title);
    expect(description).toHaveTextContent(TASK_MOCK_DATA.description);
    expect(status).toHaveTextContent(TASK_MOCK_DATA.status);
});

