import React from 'react';
import {FaEdit} from "react-icons/fa";
import {useNavigate} from "react-router-dom";

function TaskCardComponent({task, onNavigateToTask}) {
    const navigate = useNavigate();
    return (
        <div
            className={"task-card-component"}
            data-testid="task-card-component"
        >
            <div
                className="card-title"
                data-testid="card-title"
            >
                {task.title}
            </div>
            <div
                className="card-description"
                data-testid="card-description"
            >
                {task.description}
            </div>
            <div className="card-navigation">
                <div className="status">
                    <div
                        className="status-label"
                        data-testid="status-label"
                    >
                        {task.status}
                    </div>
                </div>
                <div className="edit">
                    <button
                        data-testid="navigate-to-task"
                        onClick={() => {
                            onNavigateToTask();
                            navigate(`edit/${task.id}`)
                        }}
                    >
                        <FaEdit/>
                    </button>
                </div>
            </div>
        </div>
    );
}

export default TaskCardComponent;
