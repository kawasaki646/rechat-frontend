import React, {useRef} from 'react';
import {FaPlus} from "react-icons/fa";

function AddNewTaskComponent({onAddNewTask}) {
    const titleRef = useRef<any>(null);
    const descriptionRef = useRef<any>(null);
    const formRef = useRef(null);
    return (
        <div
            id="add-new-task-form"
            data-testid="add-new-task-form"
            className="form"
        >
            <form ref={formRef}>
                <fieldset>
                    <legend className="title">Add a new Task</legend>
                    <p>
                        <input
                            name="title-field"
                            id="title-field"
                            className="field input"
                            ref={titleRef}
                            placeholder={"Title"}
                        />
                    </p>
                    <p>
                        <textarea
                            id="description-field"
                            name="description-filed"
                            className="field textarea"
                            rows={5}
                            cols={33}
                            ref={descriptionRef}
                            placeholder={"Description"}
                        />
                    </p>
                    <p>
                        <button
                            type="submit"
                            id="add-new-task"
                            className="field submit"
                            onClick={(e) => {
                                e.preventDefault();
                                onAddNewTask({
                                    id: + new Date(),
                                    title: titleRef && titleRef.current ? titleRef.current['value'] : null,
                                    description: descriptionRef && descriptionRef.current ? descriptionRef.current['value'] : null,
                                    status: 'todo'
                                })
                                titleRef.current['value'] = '';
                                descriptionRef.current['value'] = '';
                            }}
                        >
                            <FaPlus/>
                            Add
                        </button>
                    </p>
                </fieldset>
            </form>
        </div>
    );
}

export default AddNewTaskComponent;
