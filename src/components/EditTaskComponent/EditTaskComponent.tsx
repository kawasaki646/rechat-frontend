import React, {useContext, useEffect, useRef, useState} from 'react';
import {FaEdit} from "react-icons/fa";
import {Link, useParams} from "react-router-dom";
import Select from 'react-select';
import {TaskContext} from "../../context/AppContext";
import {SelectOption, Task} from "../../utils/interfaces";
import {Status} from "../../utils/enums";
import {capitalizeFirstLetter, fetchStatusesForSpecificStatus} from "../../utils/helpers";

function EditTaskComponent({onEdit, onCancel}) {
    const context = useContext(TaskContext);
    const titleRef = useRef<any>(null);
    const descriptionRef = useRef<any>(null);
    const formRef = useRef(null);
    const urlParams = useParams();
    const [targetTask, setTargetTask] = useState<Task | undefined>();
    const [selectOptions, setSelectOptions] = useState<SelectOption[]>([]);
    const [selectValue, setSelectValue] = useState<SelectOption>();

    useEffect(() => {
        const targetTask: Task | undefined = context.tasks.find((task: Task) => String(task.id) === urlParams['idx']);
        setTargetTask(targetTask);
        const statuses = fetchStatusesForSpecificStatus(targetTask ? targetTask.status : '' as Status);
        const selectOptions = statuses.map(status => {
            return {
                value: status,
                label: capitalizeFirstLetter(status)
            }
        })
        setSelectOptions(selectOptions);
    }, []);

    useEffect(() => {
        if (targetTask) {
            titleRef.current.value = targetTask.title;
            descriptionRef.current.value = targetTask.description;
        }
    }, [targetTask])

    const handleSubmitNewTask = () => {
        onEdit({
            id: targetTask && targetTask.id,
            title: titleRef.current.value,
            description: descriptionRef.current.value,
            status: selectValue && selectValue.value
        })
    }

    return (
        <div
            id="edit-task-form"
            data-testid="edit-task-form"
            className="form">
            <form ref={formRef}>
                <fieldset>
                    <legend className="title">Edit task</legend>
                    <div className="field-item">
                        <input
                            name="title-field"
                            id="title-field"
                            data-testid="title-field"
                            className="field input"
                            ref={titleRef}
                            placeholder={"Title"}
                        />
                    </div>
                    <div className="field-item">
                        <textarea
                            id="description-field"
                            data-testid="description-field"
                            name="description-filed"
                            className="field textarea"
                            rows={15}
                            cols={33}
                            ref={descriptionRef}
                            placeholder={"Description"}
                        />
                    </div>
                    <div
                        className="field-item"
                        data-testid="status-field"
                        style={selectValue ? {marginBottom: '20px'} : {marginBottom: 0}}
                    >
                        <Select
                            value={selectValue}
                            onChange={(option: any) => {
                                setSelectValue(option)
                            }}
                            className="field select"
                            classNamePrefix="react-select"
                            options={selectOptions}
                        />
                    </div>
                    {!selectValue &&
                        <div className="warning-message">Pls select a status</div>
                    }
                    <div className={"form-footer"}>
                        <Link to="/" className="field submit">
                            <button
                                type="submit"
                                id="edit-new-task"
                                disabled={!selectValue}
                                data-testid="edit-new-task"
                                onClick={handleSubmitNewTask}
                            >
                                <FaEdit/>
                                Edit
                            </button>
                        </Link>
                        <Link to="/" className="field cancel">
                            <button
                                id="cancel-new-task"
                                data-testid="cancel-new-task"
                                onClick={onCancel}
                            >
                                Cancel
                            </button>
                        </Link>
                    </div>
                </fieldset>
            </form>
        </div>
    );
}

export default EditTaskComponent;
