import React, {useContext} from 'react';
import TaskCardComponent from "../TaskCardComponent/TaskCardComponent";
import {TaskContext} from "../../context/AppContext";
import {NO_TASKS_MESSAGE, SLEEP_MESSAGE} from "../../utils/constants"
import {Task} from "../../utils/interfaces";

function TasksComponent({onNavigateToTask}) {
    const context = useContext(TaskContext);
    return (
        <div
            id="tasks-component"
            data-testid="tasks-component"
        >
            <div className="header">Tasks</div>
            <div id="task-list">
                <div id="task-list-wrapper">
                    {context.tasks.length ? context.tasks.map((task: Task) => <TaskCardComponent
                            task={task}
                            key={task.id}
                            onNavigateToTask={onNavigateToTask}
                        />) :
                        <div
                            className={"no-tasks"}
                            data-testid="no-tasks"
                        >
                            <p>{NO_TASKS_MESSAGE}</p>
                            <p>{SLEEP_MESSAGE}</p>
                        </div>}
                </div>
            </div>
        </div>
    );
}

export default TasksComponent;
