import React from 'react';
import {render, screen} from '@testing-library/react';
import TasksComponent from './TasksComponent';
import {MemoryRouter} from "react-router-dom";
import {TaskContext} from "../../context/AppContext";
import {Task} from "../../utils/interfaces";
import {Nav} from "../../utils/enums";

const MOCK_CONTEXT = {tasks: [{id: 1, description: 1}, {id: 2, description: 2}] as Task[], nav: Nav.HOME as Nav};

test('Should render no tasks block in case there are no tasks', () => {
    render(
        <MemoryRouter>
            <TasksComponent
                onNavigateToTask={() => {
                }}
            />
        </MemoryRouter>
    );
    const noTasksBlock = screen.getByTestId('no-tasks');
    expect(noTasksBlock).toBeInTheDocument();
});

test('Should render tasks if they are in context', () => {
    render(
        <TaskContext.Provider value={MOCK_CONTEXT}>
            <MemoryRouter>
                <TasksComponent
                    onNavigateToTask={() => {
                    }}
                />
            </MemoryRouter>
        </TaskContext.Provider>
    );
    const tasks = screen.queryAllByTestId('task-card-component');
    expect(tasks.length).toBe(MOCK_CONTEXT.tasks.length);
});
